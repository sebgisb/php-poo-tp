<?php 
namespace Core;
// on donne à nos modèle la propriété $id
abstract class Model
{
    public ?int $id;

    public function __construct( array $data = [] )
    {
        $this->hydrate( $data );
    }

    /**
     * Hydrate l'objet à partir d'un résultat de BDD
     *
     * @param array $data Tabl assoc renvoyer par bdd
     * @return void
     */
    private function hydrate( array $data ): void
    {
        foreach( $data as $column => $value ) {
            if( property_exists( get_called_class(), $column ) ) {
                $this->$column = $value;
            }
        }
    }
}

//