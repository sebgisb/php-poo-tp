<?php 

namespace Core;
use App\Models\Reservation;
use App\Models\Equipement;
use App\models\Favoris;

use\PDO;

abstract class Repository 
{
    protected PDO $db_cnx;

    public abstract function getTable():string;

    public function __construct( PDO $pdo )
    {
        $this->db_cnx = $pdo;
    }
// //////////////////////////// CREATE

    public function create( Model $object ): ?Model
    {
        $arr_class_vars = get_object_vars( $object);
        $arr_class_vars_keys = array_keys( $arr_class_vars);

        $str_columns = implode(',', $arr_class_vars_keys);

        $arr_vars = [];
        $arr_values = [];

        foreach( $arr_class_vars_keys as $property ) {
            array_push( $arr_vars, ':' . $property);

            $arr_values[$property] = $object->$property;
        }

        $str_vars = implode(',', $arr_vars);

        $query = sprintf(
            'INSERT INTO %s (%s) Values (%s)',
            $this->getTable(),
            $str_columns,
            $str_vars
        );

        $sth = $this->db_cnx->prepare( $query );

        $sth->execute( $arr_values );

        if($sth && $sth->errorCode() === PDO::ERR_NONE){
            $object->id = $this->db_cnx->lastInsertId();

            return $object;
        }

        return null;
    }
    ////////////////////
////////////////////////////////////////////Update
public function update( Model $object ): ?Model
{
    $arr_class_vars = get_object_vars( $object ); 
    $arr_class_vars_keys = array_keys( $arr_class_vars ); 


    $arr_vars = [];

    $arr_values = [];

    foreach( $arr_class_vars_keys as $property ) {
        $arr_values[ $property ] = $object->$property;


        if( $property === 'id' ) {
            continue;
        }

        $value = sprintf( '%1$s=:%1$s', $property );
        $arr_vars[] = $value;
    }

    $str_vars = implode( ',', $arr_vars );

    $query = sprintf(
        'UPDATE %s SET %s WHERE id=:id',
        $this->getTable(),
        $str_vars
    );

    $sth = $this->db_cnx->prepare( $query );

    $sth->execute( $arr_values );

    if( $sth && $sth->errorCode() === PDO::ERR_NONE ) {
        return $object;
    }

    return null;
}


// ////////////////////////////
    protected function readAll( string $classname ): array
    {
        $arr_objects = [];

        $query = 'SELECT * FROM ' . $this->getTable();

        $sth = $this->db_cnx->query( $query );

        if( $sth && $sth->errorCode() !== PDO::ERR_NONE) {
            return $arr_objects;
        }

        while( $row = $sth->fetch() ) {
            $obj_row = new $classname( $row );
            $arr_objects[] = $obj_row;
        }

        return $arr_objects;
     
    }
// ////////////////////////////
/**
 * Recherche un mot de passe et un nom pour la connection
 *
 * @param string $username
 * @param string $password
 * @param string $classname
 * @return Model|null
 */
   protected function readName(string $username, string $password, string $classname): ?Model
   {
       $query =sprintf( 
           'SELECT * FROM `users` where `username`=:username and `password`=:password',
           $this->getTable()
        );

       $sth = $this->db_cnx->prepare($query);
       if( !$sth ){
           return null;
       }

       $sth->bindValue('username', $username, PDO::PARAM_STR);
       $sth->bindValue('password', $password, PDO::PARAM_STR);

       $sth->execute();

       if( $sth->errorCode() !== PDO::ERR_NONE) {
           return null;
       }

       $row = $sth->fetch();
       
       if( $row < 1) {
           return null;
       }

       return new $classname($row);
   }
/////////////

/**
 * recherche par id
 *
 * @param integer $id
 * @param string $classname
 * @return Model|null
 */
    protected function readById( int $id, string $classname ): ?Model
	{
		$query = sprintf(
			'SELECT * FROM %s WHERE id=:id',
			$this->getTable()
		);

		$sth = $this->db_cnx->prepare( $query );
		if( !$sth ) {
			return null;
		}

		$sth->bindValue( 'id', $id, PDO::PARAM_INT );

		$sth->execute();

		if( $sth->errorCode() !== PDO::ERR_NONE ) {
			return null;
		}

		$row = $sth->fetch();
		if( !$row ) {
			return null;
		}

		return new $classname( $row );
    }

    


// ////////////////////////////
/**
 * Recherche via l'author_id
 *
 * @param integer $author_id
 * @param string $classname
 * @return array
 */
protected function findAuthorId( int $author_id, string $classname ): array
{
    $arr_objects = [];

    $query = sprintf(
        'SELECT * FROM %s WHERE author_id=:author_id',
        $this->getTable()
    );

    $sth = $this->db_cnx->prepare( $query );
		if( !$sth ) {
			return null;
		}

        $sth->bindValue( 'author_id', $author_id, PDO::PARAM_INT );

		$sth->execute();

		if( $sth->errorCode() !== PDO::ERR_NONE ) {
			return null;
		}

    while( $row = $sth->fetch() ) {
        $obj_row = new Reservation( $row );
        $arr_objects[] = $obj_row;
    }

    return $arr_objects;

}
// ////////////////////////////
////// Cherche réservation pour un Utilisateur standard
public function readByReservation($authorid): array
{
    $arr_objects = [];
    
    $query = 'SELECT * FROM annonce INNER JOIN reservation WHERE annonce.id= reservation.annonce_id and reservation.author_id =:authorid' ;

    $sth = $this->db_cnx->prepare( $query );

    $sth->bindValue( 'authorid', $authorid, PDO::PARAM_INT );
   
	$sth->execute();

   if( $sth && $sth->errorCode() !== PDO::ERR_NONE) {
      return $arr_objects;
   }

    while( $row = $sth->fetch() ) {
        $obj_row = new Reservation( $row );
        $arr_objects[] = $obj_row;
    }

    return $arr_objects;
}

/**
 *  Recherche les réservations sur les annonces d'un Annonceur.
 *
 * @param [type] $authorid
 * @return array
 */
public function readByReservationAnnonceur($authorid): array
{
    $arr_objects = [];
    
    $query = 'SELECT * FROM annonce INNER JOIN reservation WHERE annonce.author_id=:authorid AND reservation.annonce_id=annonce.id'  ;

    $sth = $this->db_cnx->prepare( $query );

    $sth->bindValue( 'authorid', $authorid, PDO::PARAM_INT );
   
	$sth->execute();

   if( $sth && $sth->errorCode() !== PDO::ERR_NONE) {
      return $arr_objects;
   }

    // $row = $sth->fetch();
	// 	if( !$row ) {
	// 		return null;
	// 	}

	// 	return new Reservation( $row );
    while( $row = $sth->fetch() ) {
        $obj_row = new Reservation( $row );
        $arr_objects[] = $obj_row;
    }

    return $arr_objects;
}

///////////////////////////////
public function readByDateReservation($id): array
{
    $arr_objects = [];

    $query= 'SELECT date_debut, date_fin FROM reservation WHERE annonce_id=:id';


    $sth = $this->db_cnx->prepare( $query );
		if( !$sth ) {
			return null;
		}

        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

		$sth->execute();

		if( $sth->errorCode() !== PDO::ERR_NONE ) {
			return null;
		}

    while( $row = $sth->fetch() ) {
        $obj_row = new Reservation( $row );
        $arr_objects[] = $obj_row;
    }

    return $arr_objects;

}

///////////////////////////////Delete
    public function delete( int $id ): bool
    {
        $query = sprintf( 'DELETE FROM %s WHERE id=:id', $this->getTable() );
        $sth = $this->db_cnx->prepare( $query );

        if( !$sth ) {
            return false;
        }

        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

        $sth->execute();

        return $sth->errorCode() === PDO::ERR_NONE;
    }
 
    


    public function findByEquip($id): array
    {
        $arr_objects = [];
    
        $query = 'SELECT * FROM equipements JOIN liste_equipement WHERE equipements.id=liste_equipement.equipement_id AND liste_equipement.annonce_id=:id';
    
        $sth = $this->db_cnx->prepare( $query );
            if( !$sth ) {
                return null;
            }
    
            $sth->bindValue( 'id', $id, PDO::PARAM_INT );
    
            $sth->execute();
    
            if( $sth->errorCode() !== PDO::ERR_NONE ) {
                return null;
            }
    
        while( $row = $sth->fetch() ) {
            $obj_row = new Equipement( $row );
            $arr_objects[] = $obj_row;
        }
    
        return $arr_objects;
    }


}
