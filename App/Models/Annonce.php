<?php 
namespace App\Models;


use Core\Model;


class Annonce extends Model
{
    public string $author_id;
    public string $categorie_id;
    public string $title;
    public string $prix;
    public string $taille;
    public string $description;
    public string $couchages;
    public string $adresse;
    public string $ville;

}