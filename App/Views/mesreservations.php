<?php  require_once("App/include/header.php") ?>

    <?php echo $html_h1 ?>

<?php  require_once("App/include/nav.php") ?>


<div id="all-card">
    <?php if( count($all_posts) > 0): ?>

        <ul>
            <?php foreach($all_posts as $post): ?>
            
            <li>
            <div id="single-card">
                <?php echo '<h3>' . $post->title . ' </h1>'?>
           
                <?php echo '<p>Description: '.$post->description .'</p>' ?>
    
                <?php echo '<p>Nombre de place: '.$post->couchages . '</p>' ?>
        
                <?php echo '<p>Adresse: '. $post->adresse .' '. $post->ville .'</p>' ?>
         
                <?php echo '<p>Prix: '.$post->prix . '</p>' ?>
             
                <?php echo '<p>Taille: ' . $post->taille . '</p>' ?>
                
               <?php if($_SESSION['role'] == 1){
                   echo '<p>Votre bien à était réserver  du '.$post->date_debut.' au '. $post->date_fin.' </p>';
                }else{
                    if(isset($post->date_debut)){
                    echo '<p>Vous avez réserver se bien du '.$post->date_debut.' au '. $post->date_fin.' </p>';
                    }else{
                        echo '<p>Aucune reservation pour ce bien</p>';
                    }
                } ?>
                
                
            </div>
            </li>
            <?php endforeach;?>
        </ul>

            <?php else:  echo '<p> Aucune réservation. </p>'?>

    <?php endif; ?>
    </div>


        


<?php  require_once("App/include/footer.php") ?>