<?php  require_once("App/include/header.php") ?>

    <?php echo $html_h1 ?>

<?php  require_once("App/include/nav.php") ?>
        
<div id="all-card">

        <div id="single-card">

                <?php echo '<h3>' . $all_posts->title . ' </h1>'?>
           
                <?php echo '<p>Description: '.$all_posts->description .'</p>' ?>
    
                <?php echo '<p>Nombre de place: '.$all_posts->couchages . '</p>' ?>
        
                <?php echo '<p>Adresse: '. $all_posts->adresse .' '. $all_posts->ville .'</p>' ?>
         
                <?php echo '<p>Prix: '.$all_posts->prix . '</p>' ?>
             
                <?php echo '<p>Taille: ' . $all_posts->taille . '</p>' ?>

                
                <?php if( count($all_equips) > 0): ?>
                        <h5>Equipements:</h5>
                        <ul>
                                <?php foreach($all_equips as $equip): ?>

                                        <li><?php echo $equip->nom;?></li>

                                <?php endforeach; endif;?>
                         </ul>
                        

                <?php if($_SESSION['role'] != 1): ?>      
                        <form action="/td-php-poo/detailsannoncefavoris" method="POST">
                        <input type="text" name="post_id" value=" <?php echo $all_posts->id ?>"  hidden>
                        <input type="text" name="id" value=" <?php echo $_SESSION['id'] ?>"  hidden>
                        <button type="submit" >Ajouter au favoris</button></form>
                <?php endif; ?>

        </div>
</div>


        <?php  if( isset($_SESSION['role']) &&  $_SESSION['role'] == 0) :?>

            <form action="/td-php-poo/mesreservationscree" method="POST">
            <label for="date_debut"> Date debut</label>
            <input type="date" name="date_debut" placeholder="du">
            <label for="date_fin"> Date fin</label>
            <input type="date" name="date_fin" placeholder="au">
            <input type="text" name="annonce_id" value="<?php echo $all_posts->id  ?>" hidden>
            <br>
            <button type="submit">Réserver</button>
            </form>

    <?php endif; ?>    


            
    <?php if(!isset($_SESSION['user']) ): echo'<br> <p id="centered">Connectez vous pour réserver ce bien.</p>';endif; ?>

  <?php require_once("App/include/footer.php") ?>