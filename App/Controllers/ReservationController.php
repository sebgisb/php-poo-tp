<?php 

namespace App\Controllers;


use Core\Controller;
use Core\View;
use App\Models\Reservation;

class ReservationController extends Controller
{

    /**
     * Renvoie toutes les réservation de l'utilisateur connecter
     *
     * @return void
     */
    public function index(): void
    {
         $authorid= $_SESSION['id'];
         $view = new View( 'mesreservations');
         $view_data = [
             'html_title' => 'Mes réservations',
             'html_h1' => '<h1>Mes réservations</h1>',
             'all_posts' => $this->rm->getReservationRepo()->findReservation($authorid)
           
         ];
       
        $view->render( $view_data );
    }

    /**
     * Quand l'utilisateur est un Annonceur, renvoie ces propre bien fesant l'objet d'une réservation
     *
     * @return void
     */
    public function index_co():void
    {
        $authorid= $_SESSION['id'];
        $view = new View( 'mesreservations');
        $view_data = [
            'html_title' => 'Mes réservations',
            'html_h1' => '<h1>Mes réservations</h1>',
            'all_posts' => $this->rm->getReservationRepo()->readReservationAnn($authorid)
          
        ];
      
       $view->render( $view_data );
    }

    /**
     * Si la réservation échoue
     *
     * @return void
     */
    public function indexresa():void
    {
        $authorid= $_SESSION['id'];
        $view = new View( 'mesreservations');
        $view_data = [
            'html_title' => 'Mes réservations',
            'html_h1' => '<h1>Mes réservations</h1>',
            'all_posts' => $this->rm->getReservationRepo()->readReservationAnn($authorid)
          
        ];
      
     
      
       $view->render( $view_data );
    }

    


/**
 * Création réservation et test pour voir si le bien n'est pas déja loué
 *
 * @return void
 */
    public function createResa(): void
    {   
        $reservation = new Reservation();
        $reservation->author_id = $_SESSION['id'];
        $reservation->annonce_id = $_POST['annonce_id'];
        $reservation->date_debut = $_POST['date_debut'];
        $reservation->date_fin = $_POST['date_fin'];
        $id = $reservation->annonce_id;
        
        $all_resas = $this->rm->getReservationRepo()->readDateReservation($id);

        foreach($all_resas as $resa): 
    
            if( mktime($_POST['date_fin']) > mktime($resa->date_debut) || mktime($_POST['date_debut']) < mktime($resa->date_fin))
            {
                continue;
                
            }else
            {
                header('location:/td-php-poo/mesreservationsfalse');
                exit();
            }

        endforeach; 

        $this->rm->getReservationRepo()->creation($reservation);

        header('location:/td-php-poo/mesreservations');
    }
}
