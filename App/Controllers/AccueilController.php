<?php 

namespace App\Controllers;


use Core\Controller;
use Core\View;


class AccueilController extends Controller
{   
   public function index(): void
   {
       $view = new View( 'accueil');

       $view_data = [
           'html_title' => 'Accueil',
           'html_h1' => '<h1>Accueil</h1>',
       ];

       $view->render( $view_data );
   }
}