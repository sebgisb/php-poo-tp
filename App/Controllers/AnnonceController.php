<?php 

namespace App\Controllers;

use Core\Controller;
use Core\View;
use  App\Models\Annonce;
use App\Models\Image;
use App\Models\ListeEquipement;

class AnnonceController extends Controller
{
    /**
     * Liste toutes les annonces
     *
     * @return void
     */
   public function index(): void
   {
       $view = new View( 'annonce');
 
       $view_data = [
           'html_title' => 'Les annonces',
           'html_h1' => '<h1>Les annonces</h1>',
           'all_posts' => $this->rm->getAnnonceRepo()->findAll(),
           
       ];
       $view->render( $view_data );
   }
   
   /**
    * Renvoie la liste des annonces créer par l'annonceur
    *
    * @return void
    */
   public function index_co(): void
   {
        $authorid= $_SESSION['id'];
        
        $view = new View( 'annonce');
        $view_data = [
            'html_title' => 'Mes annonces',
            'html_h1' => '<h1>Mes annonces</h1>',
            'all_posts' => $this->rm->getAnnonceRepo()->findauthor_id($authorid),
            'all_equips' => $this->rm->getEquipementRepo()->findAll()
            
        ];

       $view->render( $view_data );
   }
   

   /**
    * Création d'une annonce
    *
    * @return void
    */
   public function createAnnonce():void 
   {
        $annonce = new Annonce();
        $annonce->author_id = $_SESSION['id'];
        $annonce->categorie_id =$_POST['categorie_id'];
        $annonce->title = $_POST['title'];
        $annonce->prix = $_POST['prix'];
        $annonce->taille = $_POST['taille'];
        $annonce->description =$_POST['description'];
        $annonce->couchages = $_POST['couchages'];
        $annonce->adresse = $_POST['adresse'];
        $annonce->ville = $_POST['ville'];

        $this->rm->getAnnonceRepo()->creation($annonce);

    
    foreach($_POST['equipements'] as $id_equipements){       
            $equipement= new ListeEquipement();
            $equipement->annonce_id = $annonce->id;
            $equipement->equipement_id = $id_equipements; //$id_equipements;
            $this->rm->getListeEquipementRepo()->creation($equipement);
        }
        
        header('location:/td-php-poo/annonce');
        exit();
    }



    
    
    
   
}