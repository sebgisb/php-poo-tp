<?php 

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\Favoris;

class DetailsController extends Controller
{

    /**
     * Détails d'une annonce
     *
     * @return void
     */
   public function index(): void
   {
       $id= $_POST['id'];
       $view = new View('detailsannonce');
        
       $view_data = [
           'html_title' => 'Détails',
           'html_h1' => '<h1>Détails</h1>',
           'all_posts' => $this->rm->getAnnonceRepo()->findId($id),
           'all_equips' => $this->rm->getListeEquipementRepo()->findEquip($id)
        //    'all_resas' => $this->rm->getReservationRepo()->readDateReservation($id)
       ];

       $view->render( $view_data );
   }
   

  public function favoris():void
  {
    $favoris = new Favoris();
    $favoris->user_id = $_POST['id'];
    $favoris->annonce_id = $_POST['post_id'];

    $this->rm->getFavorisRepo()->creation($favoris);
    }

}

