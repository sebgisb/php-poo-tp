<?php 

namespace App\Controllers;
//////////

//////////
use Core\Controller;
use Core\View;


class ConnexionController extends Controller
{

    
   public function index(): void
   {
       $view = new View( 'connexion');

       $view_data = [
           'html_title' => 'Se connecter',
           'html_h1' => '<h1>Connexion</h1>'
       ];

       $view->render( $view_data );
   }



   public function connexion(): void
   {
        $username = $_POST['username'];
        $password = hash('sha256', $_POST['password']);
        $user=$this->rm->getUserRepo()->findName($username, $password);

        if(!is_null($user)){

            $_SESSION['user'] = $username;
            $_SESSION['role'] = $user->role_id;
            $_SESSION['id'] = $user->id;
            
            var_dump($_SESSION);
            echo 'Vous êtes connecter en tant que '. $_SESSION['user'];
            header('location:/td-php-poo/');
            exit();
        }else{
            echo'Erreur de connexion';
        }     
   }



   public function deconnexion():void{
        session_destroy();
        header('location:/td-php-poo/');
        exit();
   }

}

