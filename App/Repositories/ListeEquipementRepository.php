<?php 
namespace App\Repositories;

use Core\Repository;
use App\Models\ListeEquipement;

class ListeEquipementRepository extends Repository
{
    public function getTable(): string 
    {
        return 'liste_equipement';
    }

    public function findAll(): array
    {
        return $this->readAll( ListeEquipement::class);
    }
/**
 * Liste les équipements d'une annonce
 *
 * @param integer $id
 * @return array
 */
    public function findEquip( int $id): array
    {
        return $this->findByEquip($id, ListeEquipement::class);
    }

    public function creation($equipement): void
    {
        $this->create($equipement);
    }

}