<?php 

namespace App\Repositories;

use Core\Database;

class RepositoryManager
{
    private static ?self $instance = null;
//////////////////////
    private UserRepository $user_repo;
    public function getUserRepo(): UserRepository
    {
        return $this->user_repo;
    }
//////////////////////
private AnnonceRepository $annonce_repo;
    public function getAnnonceRepo(): AnnonceRepository
    {
        return $this->annonce_repo;
    }
//////////////////////
private ReservationRepository $reservation_repo;
    public function getReservationRepo(): ReservationRepository
    {
        return $this->reservation_repo;
    }
/////////////////////////
private EquipementRepository $equipement_repo;
    public function getEquipementRepo(): EquipementRepository
    {
        return $this->equipement_repo;
    }
/////////////////////////
private FavorisRepository $favoris_repo;
    public function getFavorisRepo(): FavorisRepository
    {
        return $this->favoris_repo;
    }
/////////////////////////
private ListeEquipementRepository $liste_equipement_repo;
    public function getListeEquipementRepo(): ListeEquipementRepository
    {
        return $this->liste_equipement_repo;
    }

/////////////////////////
    public static function getRm(): self
    {
        if( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $pdo = Database::get();

        $this->user_repo = new UserRepository( $pdo );
        $this->annonce_repo = new AnnonceRepository( $pdo );
        $this->reservation_repo = new ReservationRepository( $pdo );
        $this->equipement_repo = new EquipementRepository( $pdo );
        $this->liste_equipement_repo = new ListeEquipementRepository( $pdo );
        $this->favoris_repo = new FavorisRepository( $pdo );
   
       
    }

    private function __clone() { }
    private function __wakeup() { }
}