<?php 
namespace App\Repositories;

use Core\Repository;
use App\Models\Annonce;

class AnnonceRepository extends Repository
{
    public function getTable(): string 
    {
        return 'annonce';
    }

    public function findAll(): array
    {
        return $this->readAll( Annonce::class);
    }

    public function findId( int $id ): ?Annonce
    {
        return $this->readById($id,Annonce::class);
    }

    public function findauthor_id( int $authorid): array
    {
        return $this->findAuthorId($authorid, Annonce::class);
    }

    public function creation($annonce): void
    {
        $this->create($annonce);
    }

    public function updateAnnonce($annonce) :void
    {
        $this->update($annonce);
    }
   
}