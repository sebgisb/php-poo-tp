<?php 
namespace App\Repositories;

use Core\Model;
use Core\Repository;
use App\Models\User;

class UserRepository extends Repository
{
    public function getTable(): string 
    {
        return 'users';
    }

    public function findAll(): array
    {
        return $this->readAll( User::class);
    }

    public function creation($user): void
    {
        $this->create($user);
    }

    /**
     * Connexion de l'utilisateur
     *
     * @param string $username
     * @param string $password
     * @return Model|null
     */
    public function findName(string $username, string $password): ?Model
    {
        return $this->readName($username,$password, User::class);
    }
    

}