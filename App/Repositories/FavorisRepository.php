<?php 
namespace App\Repositories;

use Core\Repository;
use App\Models\Favoris;

class FavorisRepository extends Repository
{
    public function getTable(): string 
    {
        return 'favoris';
    }

    public function findAll(): array
    {
        return $this->readAll( Favoris::class);
    }

    public function creation($favoris): void
    {
        $this->create($favoris);
    }

}