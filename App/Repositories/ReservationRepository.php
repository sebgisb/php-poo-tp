<?php 
namespace App\Repositories;

use Core\Repository;
use App\Models\Reservation;

class ReservationRepository extends Repository
{
    public function getTable(): string 
    {
        return 'reservation';
    }

    public function creation($reservation): void
    {
        $this->create($reservation);
    }
/**
 * liste de toutes les résérvation pour un utilisateur 
 *
 * @param integer $authorid
 * @return array
 */
    public function findReservation(int $authorid): array
    {
        return $this->readByReservation( $authorid, Reservation::class);
    }

    /**
     * Liste des réservation pour une annonce
     *
     * @param integer $authorid
     * @return array
     */
    public function readReservationAnn(int $authorid): array
    {
        return $this->readByReservationAnnonceur( $authorid, Reservation::class);
    }

    /**
     * Date des reservation
     *
     * @param integer $id
     * @return array
     */
    public function readDateReservation(int $id):array
    {
    return $this->readByDateReservation( $id, Reservation::class);
    }
}