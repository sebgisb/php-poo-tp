<?php 
namespace App\Repositories;

use Core\Repository;
use App\Models\Equipement;

class EquipementRepository extends Repository
{
    public function getTable(): string 
    {
        return 'equipements';
    }

    public function findAll(): array
    {
        return $this->readAll( Equipement::class);
    }

}