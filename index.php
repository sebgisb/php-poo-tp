<?php
session_start();

define('SESSION_USER', 'user');
define('SESSION_ROLE', 'role');
define('SESSION_ID','id');
define('SESSION_POST_ID','post_id');

// var_dump($_SESSION);

// Paramètres Base de données
define( 'DB_HOST', 'localhost' );
define( 'DB_NAME', 'td-php-poo' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );

// Paramètres PDO
define( 'PDO_ENGINE', 'mysql' );
define( 'PDO_OPTIONS', [
	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname(__FILE__) . DS );

use Core\Router;

spl_autoload_register();

$router = Router::get();
$router->controllers_namespace = '\App\Controllers';

///////////Les pages

//Accueil
$router->registerRoute('/td-php-poo/', 'AccueilController', 'index');

// Annonces
if(isset($_SESSION['role']) && $_SESSION['role'] == 1 ){
	// Liste des annonces si admin
	$router->registerRoute('/td-php-poo/annonce', 'AnnonceController', 'index_co');
}else{
	// Liste des annonces tous
	$router->registerRoute('/td-php-poo/annonce', 'AnnonceController', 'index');
}

$router->registerRoute('/td-php-poo/detailsannoncefavoris', 'AnnonceController', 'index');


//Création nouvelle annonce
$router->registerRoute('/td-php-poo/AnnonceCreate', 'AnnonceController', 'createAnnonce');


//Détails annonce
$router->registerRoute('/td-php-poo/detailsannonce', 'DetailsController', 'index');

$router->registerRoute('/td-php-poo/detailsannoncefavoris', 'AnnonceController', 'favoris');



// Page connexion
$router->registerRoute('/td-php-poo/connexion', 'ConnexionController','index');
// connexion
$router->registerRoute('/td-php-poo/gestionannonce', 'ConnexionController','connexion');
// deconnexion
$router->registerRoute('/td-php-poo/deconnexion', 'ConnexionController','deconnexion');


// Inscription
$router->registerRoute('/td-php-poo/inscription', 'InscriptionController','index');
//creation utilisateur
$router->registerRoute('/td-php-poo/inscriptiontrue', 'InscriptionController','createUser');


//Réservation
if( isset($_SESSION['role']) && $_SESSION['role'] == 1){
	// Si Annonceur 
	$router->registerRoute('/td-php-poo/mesreservations', 'ReservationController','index_co');
}else{
	$router->registerRoute('/td-php-poo/mesreservations', 'ReservationController','index');
}
$router->registerRoute('/td-php-poo/mesreservationsfalse', 'ReservationController', 'indexresa');
//Réserver
$router->registerRoute('/td-php-poo/mesreservationscree', 'ReservationController','createResa');

$router->start();
	
